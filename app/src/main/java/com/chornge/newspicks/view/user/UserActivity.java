package com.chornge.newspicks.view.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.chornge.newspicks.R;
import com.chornge.newspicks.injection.user.DaggerUserComponent;
import com.squareup.picasso.Picasso;
import com.szugyi.circlemenu.view.CircleImageView;
import com.szugyi.circlemenu.view.CircleLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserActivity extends AppCompatActivity implements UserContract.View {

    @Inject
    UserPresenter presenter;

    @BindView(R.id.circle_user_menu)
    CircleLayout circleLayout;

    @BindView(R.id.noon_position_image)
    CircleImageView firstImage;

    @BindView(R.id.two_position_image)
    CircleImageView secondImage;

    @BindView(R.id.four_position_image)
    CircleImageView thirdImage;

    @BindView(R.id.six_position_image)
    CircleImageView fourthImage;

    @BindView(R.id.eight_position_image)
    CircleImageView fifthImage;

    @BindView(R.id.ten_position_image)
    CircleImageView sixthImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        ButterKnife.bind(this);
        DaggerUserComponent.create().inject(this);

        presenter.addView(this);
        presenter.loadFaceIcons();
    }

    @Override
    public void showIcons() {

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100013_photo_url))
                .into(firstImage);

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100019_photo_url))
                .into(secondImage);

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100094_photo_url))
                .into(thirdImage);

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100269_photo_url))
                .into(fourthImage);

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100353_photo_url))
                .into(fifthImage);

        Picasso.with(this)
                .load(getResources().getString(R.string.user_100529_photo_url))
                .into(sixthImage);
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter.addView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        presenter.removeView();
    }

    @Override
    public void showError(String error) {
        //
    }
}
