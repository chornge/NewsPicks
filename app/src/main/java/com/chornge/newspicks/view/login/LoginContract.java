package com.chornge.newspicks.view.login;

import com.chornge.newspicks.BasePresenter;
import com.chornge.newspicks.BaseView;

public interface LoginContract {

    interface View extends BaseView {
        void enableLoginButton();

        void disableLoginButton();

        void hideKeyboard();

        void startNetworkService();

        void showFailedAuthentication();

        void navigateToUserScreen();
    }

    interface Presenter extends BasePresenter<View> {
        void logUserIn();
    }
}
