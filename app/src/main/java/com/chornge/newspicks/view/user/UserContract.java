package com.chornge.newspicks.view.user;

import com.chornge.newspicks.BasePresenter;
import com.chornge.newspicks.BaseView;

public interface UserContract {
    interface View extends BaseView {
        void showIcons();
    }

    interface Presenter extends BasePresenter<View> {
        void loadFaceIcons();
    }
}
