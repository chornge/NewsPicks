package com.chornge.newspicks.view.login;

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;

    @Override
    public void addView(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
    }

    @Override
    public void logUserIn() {
        view.disableLoginButton();

        view.startNetworkService();

//        Random random = new Random();
//        if (random.nextInt() % 2 == 0) {
//            view.navigateToUserScreen();
//        } else {
//            view.showFailedAuthentication();
//        }
    }
}
