package com.chornge.newspicks.view.user;

public class UserPresenter implements UserContract.Presenter {

    private UserContract.View view;

    @Override
    public void addView(UserContract.View view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
    }

    @Override
    public void loadFaceIcons() {
        view.showIcons();
    }
}
