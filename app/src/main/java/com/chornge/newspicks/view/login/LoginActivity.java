package com.chornge.newspicks.view.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chornge.newspicks.R;
import com.chornge.newspicks.injection.login.DaggerLoginComponent;
import com.chornge.newspicks.model.FailureResponse;
import com.chornge.newspicks.model.NetworkResponse;
import com.chornge.newspicks.model.SuccessResponse;
import com.chornge.newspicks.network.LoginService;
import com.chornge.newspicks.view.user.UserActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

// TODO: Refactor to check that @ and . are included in email entry.

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @Inject
    LoginPresenter presenter;

    @BindView(R.id.login_button)
    Button loginButton;

    @BindView(R.id.user_email_entry)
    TextView emailEntry;

    @BindView(R.id.user_password_entry)
    TextView passwordEntry;

    @BindView(R.id.login_progress_bar)
    ProgressBar progressBar;

    private NetworkResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        DaggerLoginComponent.create().inject(this);
        presenter.addView(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                checkForEmptyFields();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //
            }
        };

        emailEntry.addTextChangedListener(textWatcher);
        passwordEntry.addTextChangedListener(textWatcher);
        loginButton.setOnClickListener(view -> {
            hideKeyboard();
            showProgressBar();
            presenter.logUserIn();
        });
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void checkForEmptyFields() {
        String s1 = emailEntry.getText().toString();
        String s2 = passwordEntry.getText().toString();

        if (s1.length() > 0 && s2.length() > 0) {
            enableLoginButton();
        } else {
            disableLoginButton();
        }
    }

    @Override
    public void enableLoginButton() {
        loginButton.setEnabled(true);
        loginButton.setBackground(getResources().getDrawable(R.color.orange));
    }

    @Override
    public void disableLoginButton() {
        loginButton.setEnabled(false);
        loginButton.setBackground(getResources().getDrawable(R.color.yellow));
    }

    @Override
    public void hideKeyboard() {
        View view = getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            //noinspection ConstantConditions
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void startNetworkService() {
        disableLoginButton();
        Intent serviceIntent = new Intent(this, LoginService.class);
        serviceIntent.putExtra("email_hash", emailEntry.getText());
        serviceIntent.putExtra("password_hash", passwordEntry.getText());
        startService(serviceIntent);
    }

    @Override
    public void navigateToUserScreen() {
        hideProgressBar();
        emailEntry.setText("");
        passwordEntry.setText("");

        response = null;
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    private void showToastMessage(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResponseReceived(NetworkResponse response) {
        this.response = response;

        if (response instanceof SuccessResponse) {
            showToastMessage(response.getMessage());
            navigateToUserScreen();
        } else if (response instanceof FailureResponse) {
            showToastMessage(response.getMessage());
            enableLoginButton();
            hideProgressBar();
        }
    }

    @Override
    public void showFailedAuthentication() {
        if (response != null) {
            showToastMessage(response.getMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        emailEntry.requestFocus();

        presenter.addView(this);

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        emailEntry.setText("");
        passwordEntry.setText("");
        EventBus.getDefault().unregister(this);

        presenter.removeView();
    }

    @Override
    public void showError(String error) {
        //
    }
}
