package com.chornge.newspicks.injection.login;

import com.chornge.newspicks.view.login.LoginPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    LoginPresenter providesLoginPresenter() {
        return new LoginPresenter();
    }
}
