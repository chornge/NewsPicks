package com.chornge.newspicks.injection.login;

import com.chornge.newspicks.view.login.LoginActivity;

import dagger.Component;

@Component(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
