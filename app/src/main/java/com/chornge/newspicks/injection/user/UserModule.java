package com.chornge.newspicks.injection.user;

import com.chornge.newspicks.view.user.UserPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    UserPresenter providesUserPresenter() {
        return new UserPresenter();
    }
}
