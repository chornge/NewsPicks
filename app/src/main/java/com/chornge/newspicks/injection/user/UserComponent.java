package com.chornge.newspicks.injection.user;

import com.chornge.newspicks.view.user.UserActivity;

import dagger.Component;

@Component(modules = UserModule.class)
public interface UserComponent {
    void inject(UserActivity userActivity);
}