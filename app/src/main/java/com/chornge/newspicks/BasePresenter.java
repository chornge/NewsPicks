package com.chornge.newspicks;

public interface BasePresenter<V extends BaseView> {
    void addView(V view);

    void removeView();
}
