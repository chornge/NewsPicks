package com.chornge.newspicks.model;

public class NetworkRequest {
    private String email;
    private String password;

    public NetworkRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public String toString() {
        return "NetworkRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
