package com.chornge.newspicks.model;

public class NetworkResponse {
    private NetworkRequest request;

    NetworkResponse(NetworkRequest request) {
        this.request = request;
    }

    public String getMessage() {
        return "response";
    }

    public NetworkRequest getRequest() {
        return request;
    }
}
