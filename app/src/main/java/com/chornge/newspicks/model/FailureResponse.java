package com.chornge.newspicks.model;

public class FailureResponse extends NetworkResponse {

    private String message;

    public FailureResponse(NetworkRequest request) {
        super(request);
        this.message = "Login failed";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
