package com.chornge.newspicks.model;

public class SuccessResponse extends NetworkResponse {

    private String message;

    public SuccessResponse(NetworkRequest request) {
        super(request);
        this.message = "Login success!";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
