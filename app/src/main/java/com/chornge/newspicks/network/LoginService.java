package com.chornge.newspicks.network;

import android.app.IntentService;
import android.content.Intent;

import com.chornge.newspicks.model.FailureResponse;
import com.chornge.newspicks.model.NetworkRequest;
import com.chornge.newspicks.model.NetworkResponse;
import com.chornge.newspicks.model.SuccessResponse;

import org.greenrobot.eventbus.EventBus;

import java.util.Random;

/**
 * Stub API to return 'success' or 'failure' for email and password requests.
 * <p>
 * By default, every email and password combination gives a success result, except when both are the
 * same value; i.e email is 'h' and password is 'h'
 */
public class LoginService extends IntentService {

    private NetworkResponse response;

    public LoginService() {
        super("LoginService");
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onHandleIntent(Intent intent) {
        String emailToAuthenticate = String.valueOf(intent.getExtras().get("email_hash"));
        String passwordToAuthenticate = String.valueOf(intent.getExtras().get("password_hash"));

        Random random = new Random();
        int timeProcessingRequest = random.nextInt((4000 - 1000) + 1000); // wait 1 to 4 seconds.

        if (emailToAuthenticate.equals(passwordToAuthenticate)) {
            response = new FailureResponse(new NetworkRequest(emailToAuthenticate, passwordToAuthenticate));
        } else {
            response = new SuccessResponse(new NetworkRequest(emailToAuthenticate, passwordToAuthenticate));
        }

        try {
            Thread.sleep(timeProcessingRequest);
            EventBus.getDefault().post(response);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
